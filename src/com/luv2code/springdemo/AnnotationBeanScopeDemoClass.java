package com.luv2code.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AnnotationBeanScopeDemoClass {

	public static void main(String[] args) {

		// Load Spring config file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		// Get the bean from the spring container
		Coach theCoach = context.getBean("tennisCoach", Coach.class);
		
		// Check if they are the same
		System.out.println(theCoach.getDailyWorkout());
		System.out.println((theCoach.getDailyFortune()));
		
		// Close the context
		context.close();

	}

}
