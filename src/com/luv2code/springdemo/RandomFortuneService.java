package com.luv2code.springdemo;

import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class RandomFortuneService implements FortuneService {

	String[] fortunes = {"Today is your lucky day", "Today is an unlucky day", "Today is a normal day"};

	@Override
	public String getFortune() {
		// TODO Auto-generated method stub
		return getRandom(fortunes);
	}
	
	public String getRandom(String[] array) {
	    int rnd = new Random().nextInt(array.length);
	    return array[rnd];
	}

}
