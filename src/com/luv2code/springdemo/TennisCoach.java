package com.luv2code.springdemo;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope("singleton")
@Component
public class TennisCoach implements Coach {
	
	@Qualifier("randomFortuneService")
	@Autowired
	private FortuneService fortuneService;
	
	public TennisCoach () {
		//System.out.println(">> TennisCoach: inside default constructor");
	}
	
	/*
	@Autowired
	public TennisCoach(FortuneService theFortuneService) {
		fortuneService = theFortuneService;
	}
	*/
	
	/*@Autowired
	public void setFortuneService(FortuneService fortuneService) {
		this.fortuneService = fortuneService;
		System.out.println(">> TennisCoach: inside setFortuneService() method");
	}
	*/
	public FortuneService getFortuneService() {
		return fortuneService;
	}


	@Override
	public String getDailyWorkout() {
		return "Practice your backhand volley";
	}

	@Override
	public String getDailyFortune() {
		// TODO Auto-generated method stub
		return fortuneService.getFortune();
	}
	
	//@PostConstruct
	private void initMethod() {
		System.out.println("Initializing Tennis Coach...");
	}
	
	//@PreDestroy
	private void destroyMethod() {
		System.out.println("Destroying...");
	}

}
